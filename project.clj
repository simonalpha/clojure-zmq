(defproject zmq "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.5.1"]
                 [org.zeromq/jzmq "2.1.0-SNAPSHOT"]]
  :profiles {:dev {:jvm-opts ["-Djava.library.path=/usr/local/lib"]}}
  :main zmq.core)
